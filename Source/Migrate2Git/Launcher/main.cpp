// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "Core/Application/Application.h"


int main( int argc, char* argv[], char* envp[] )
{
    std::atexit( []()
    {
        system( "pause" );
    } );

    core::Application app{ argc, argv, envp };
    app.run();

    return 0;
}
