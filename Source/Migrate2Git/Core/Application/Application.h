#pragma once


#include "CppGit/Library.h"


namespace core
{
    
    class Application final
    {
    public:
        Application( int argc, char *argv[], char *envp[] = nullptr );
        ~Application();
        Application( const Application& ) = delete;
        Application& operator=( const Application& ) = delete;

        void run();

    private:
        git::Library _git;
    };

}