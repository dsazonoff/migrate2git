#pragma once

// Algorithms
#include <functional>
#include <algorithm>

// Types
#include <type_traits>
#include <chrono>
#include <string>

// Containers
#include <array>
#include <vector>
#include <map>
#include <set>

// Streams
#include <iostream>

// Memory
#include <memory>

// Misc
#include <cassert>
#include <cstdlib>

// Threads
#include <thread>
#include <mutex>
#include <future>

// File system
#include <filesystem>
namespace std
{
    namespace filesystem = experimental::filesystem;
}
