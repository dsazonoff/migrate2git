@echo off

call :AbsolutePath ..\..\3rdparty\libgit2\_build
set BUILD_PREFIX=%RETVAL%
set NAME_PREFIX=libgit2

call :RunCMake "Visual Studio 15 2017" "x86"
call :RunCMake "Visual Studio 15 2017 Win64" "x64"

goto :EOF
rem -----------------------------------------------

:AbsolutePath
set RETVAL=%~dpfn1
exit /b

:RunCMake
set BUILD_PATH=%BUILD_PREFIX%_%2
set LIB_NAME=%NAME_PREFIX%_%2
if not exist %BUILD_PATH% md %BUILD_PATH%
cd %BUILD_PATH%
cmake -DBUILD_CLAR=OFF -DBUILD_SHARED_LIBS=OFF -DLIBGIT2_FILENAME=%LIB_NAME% -DTHREADSAFE=ON -DSTATIC_CRT=OFF -G %1 ..
exit /b
