﻿#pragma once


namespace git
{
 
    class Exception
        : public std::exception
    {
    public:
        enum class Category
        {
            None,
        };

    public:
        Exception();
        Exception( std::string_view message, Category category );
        ~Exception();

        const char *what() const override;

    private:
        Category _category = Category::None;
        std::string _message;
    };


    inline int gitThrow( int ret )
    {
        if ( ret < 0 )
            throw Exception{};
        return ret;
    }

}
