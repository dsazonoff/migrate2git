﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com


#include "Library.h"


namespace git
{

    Library::Library()
    {
        git_libgit2_init();
    }

    Library::~Library()
    {
        git_libgit2_shutdown();
    }

}
