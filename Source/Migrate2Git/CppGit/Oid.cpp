﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "Oid.h"
#include "Exception.h"


namespace git
{

    namespace
    {
        const std::array<std::byte, Oid::RawSize> empty{};
    }

    Oid::Oid( const git_oid* oid )
    {
        if ( oid == nullptr )
        {
            _d = empty;
            return;
        }

        git_oid_cpy( reinterpret_cast<git_oid*>(_d.data()), oid );
    }

    bool Oid::isValid() const
    {
        const auto isZeroFilled = _d == empty;
        return !isZeroFilled;
    }

    git_oid* Oid::data()
    {
        return reinterpret_cast<git_oid *>(_d.data());
    }

    const git_oid* Oid::data() const
    {
        return reinterpret_cast<const git_oid *>(_d.data());
    }

    Oid Oid::fromHex( std::string_view str )
    {
        Oid oid;
        gitThrow( git_oid_fromstrn( oid.data(), str.data(), str.length() ) );
        return oid;
    }

}
