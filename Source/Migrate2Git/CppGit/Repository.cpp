﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com


#include "Repository.h"

#include "Exception.h"


namespace git
{

    void Repository::init( const std::filesystem::path& path, bool isBare )
    {
        _p.reset();
        git_repository *repo = nullptr;
        gitThrow( git_repository_init( &repo, path.string().c_str(), isBare ) );
        setRepo( repo, path );
    }

    void Repository::open( const std::filesystem::path& path )
    {
        _p.reset();
        git_repository *repo = nullptr;
        gitThrow( git_repository_open( &repo, path.string().c_str() ) );
        setRepo( repo, path );
    }

    void Repository::setRepo( git_repository* repo, const std::filesystem::path& path )
    {
        _p = std::shared_ptr<git_repository>( repo, git_repository_free );
        _path = path;
    }

}
