﻿#pragma once



namespace git
{
    
    class Oid
    {
    public:
        enum
        {
            RawSize = GIT_OID_RAWSZ,
            HexSize = GIT_OID_HEXSZ,
        };

    public:
        explicit Oid( const git_oid* oid = nullptr );

        bool isValid() const;
        git_oid* data();
        const git_oid* data() const;

    public:
        static Oid fromHex( std::string_view str );

    private:
        std::array<std::byte, RawSize> _d;
    };

}
