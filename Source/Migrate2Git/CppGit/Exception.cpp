﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "Exception.h"


namespace git
{

    Exception::Exception()
    {
        const auto error = giterr_last();
        if ( error != nullptr )
        {
            _message = error->message;
            switch ( error->klass )
            {
            case GITERR_NONE:
                _category = Category::None;
                break;
            default:
                assert( false );
                break;
            }
        }

        giterr_clear();
    }

    Exception::Exception( std::string_view message, Category category )
        : _category{ category }
        , _message{ message }
    {
    }

    Exception::~Exception()
    {
    }

    const char* Exception::what() const
    {
        return _message.c_str();
    }

}
