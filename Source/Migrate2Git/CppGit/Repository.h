﻿#pragma once


namespace git
{
    
    class Repository
    {
    public:
        Repository() = default;
        ~Repository() = default;

        void init( const std::filesystem::path& path, bool isBare );
        void open( const std::filesystem::path& path );


    private:
        void setRepo( git_repository *repo, const std::filesystem::path& path );

    private:
        std::shared_ptr<git_repository> _p;
        std::filesystem::path _path;
    };

}
