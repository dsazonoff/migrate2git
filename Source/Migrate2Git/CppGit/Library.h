﻿#pragma once


namespace git
{
    
    class Library final
    {
    public:
        Library();
        ~Library();
        Library( const Library& ) = delete;
        Library& operator=( const Library& ) = delete;

    private:
    };

}
